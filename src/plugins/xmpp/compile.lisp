(require 'cmp)

(defun cmpsupport (source afile)
  (let* ((src-length (length source))
	 ;; HACK(eta): what the heck
	 ;; TODO(eta): sterni says if you give it a pathname this hack
	 ;; isn't required?
	 (c::+static-library-format+ "~a.a")
	 (cursed-obj-file (concatenate 'string
				       (subseq source 0 (- src-length 4))
				       "o")))
    (multiple-value-bind (outname warnings-p failure-p)
      (compile-file source :system-p t)
      (when (or warnings-p failure-p)
	(quit 1)))
    (c::build-static-library afile
			     :lisp-files (list cursed-obj-file)
			     :init-name "ecl_support_init")
    (quit)))
