/*
 * xmpp.c - incredibly cursed XMPP plugin
 *
 * Copyright (C) 2021 eta <git@eta.st>
 *
 * This file is part of WeeChat, the extensible chat client.
 *
 * WeeChat is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * WeeChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WeeChat.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <strophe.h>
#include <ecl/ecl.h>

#include "../weechat-plugin.h"
#include "xmpp.h"

#define LISP_TRY ECL_RESTART_CASE_BEGIN(ecl_process_env(), ecl_make_symbol("ABORT", "CL"))
#define LISP_CATCH ECL_RESTART_CASE(1, __args__)
#define LISP_END_TRY ECL_RESTART_CASE_END

/* Interval at which to call the libstrophe event loop */
const long EVENT_LOOP_INTERVAL = 200;
struct t_weechat_plugin *weechat_xmpp_plugin = NULL;
struct t_config_option *opt_jid = NULL;
struct t_config_option *opt_password = NULL;
struct t_config_option *opt_autoconnect = NULL;
struct t_config_option *opt_autoload = NULL;
xmpp_ctx_t *xmpp_ctx = NULL;
xmpp_conn_t *xmpp_conn = NULL;

cl_object support_connect_func = ECL_NIL;
cl_object stanza_handler_func = ECL_NIL;
cl_object log_handler_func = ECL_NIL;

WEECHAT_PLUGIN_NAME(XMPP_PLUGIN_NAME);
WEECHAT_PLUGIN_DESCRIPTION(N_("Cursed XMPP plugin"));
WEECHAT_PLUGIN_AUTHOR("eta <git@eta.st>");
WEECHAT_PLUGIN_VERSION(WEECHAT_VERSION);
WEECHAT_PLUGIN_LICENSE(WEECHAT_LICENSE);
WEECHAT_PLUGIN_PRIORITY(8000);

char* ecl_get_string_for(cl_object obj) {
        cl_object formatted = NULL;
        ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ECL_T)) {
                formatted = cl_princ_to_string(obj);
        } ECL_HANDLER_CASE(1, condition) {
        } ECL_HANDLER_CASE_END;
        if (formatted == NULL) {
                return "[error occurred printing Lisp object]";
        }
        assert(ECL_STRINGP(formatted));
        if (!ecl_fits_in_base_string(formatted)) {
                return "[Unicode error printing Lisp object]";
        }
        cl_object base = si_coerce_to_base_string(formatted);
        char* ret = base->base_string.self;
        return ret;
}

void strophe_log_handler(void *userdata, xmpp_log_level_t rawlevel, const char *area, const char *msg) {
  cl_object level = ECL_NIL;
  switch (rawlevel) {
  case XMPP_LEVEL_DEBUG:
    level = eclk("DEBUG");
    break;
  case XMPP_LEVEL_INFO:
    level = eclk("INFO");
    break;
  case XMPP_LEVEL_WARN:
    level = eclk("WARN");
    break;
  case XMPP_LEVEL_ERROR:
    level = eclk("ERROR");
    break;
  }
  LISP_TRY {
		cl_funcall(4, log_handler_func, level, ecls(area), ecls(msg));
	} LISP_CATCH {
		weechat_printf(NULL, "%sxmpp: log handler func failed", weechat_prefix("error"));
	} LISP_END_TRY;
}

static const xmpp_log_t xmpp_logger = {&strophe_log_handler, NULL};

int
xmpp_finish () {
	if (xmpp_conn) {
		xmpp_conn_t *old_conn = xmpp_conn;
		xmpp_conn = NULL;
		xmpp_conn_release(old_conn);
	}
	return WEECHAT_RC_OK;
}

const char* stringify_error_type (xmpp_error_type_t err) {
	if (!err) {
		return "(null)";
	}
	switch (err) {
		case XMPP_SE_BAD_FORMAT:
			return "bad-format";
		case XMPP_SE_BAD_NS_PREFIX:
			return "bad-namespace-prefix";
		case XMPP_SE_CONFLICT:
			return "conflict";
		case XMPP_SE_CONN_TIMEOUT:
			return "connection-timeout";
		case XMPP_SE_HOST_GONE:
			return "host-gone";
		case XMPP_SE_HOST_UNKNOWN:
			return "host-unknown";
		case XMPP_SE_IMPROPER_ADDR:
			return "improper-addressing";
		case XMPP_SE_INTERNAL_SERVER_ERROR:
			return "internal-server-error";
		case XMPP_SE_INVALID_FROM:
			return "invalid-from";
		case XMPP_SE_INVALID_ID:
			return "invalid-id";
		case XMPP_SE_INVALID_NS:
			return "invalid-namespace";
		case XMPP_SE_INVALID_XML:
			return "invalid-xml";
		case XMPP_SE_NOT_AUTHORIZED:
			return "not-authorized";
		case XMPP_SE_POLICY_VIOLATION:
			return "policy-violation";
		case XMPP_SE_REMOTE_CONN_FAILED:
			return "remote-connection-failed";
		case XMPP_SE_RESOURCE_CONSTRAINT:
			return "resource-constraint";
		case XMPP_SE_RESTRICTED_XML:
			return "restricted-xml";
		case XMPP_SE_SEE_OTHER_HOST:
			return "see-other-host";
		case XMPP_SE_SYSTEM_SHUTDOWN:
			return "system-shutdown";
		case XMPP_SE_UNDEFINED_CONDITION:
			return "undefined-condition";
		case XMPP_SE_UNSUPPORTED_ENCODING:
			return "unsupported-encoding";
		case XMPP_SE_UNSUPPORTED_STANZA_TYPE:
			return "unsupported-stanza-type";
		case XMPP_SE_UNSUPPORTED_VERSION:
			return "unsupported-version";
		case XMPP_SE_XML_NOT_WELL_FORMED:
			return "xml-not-well-formed";
		default:
			return "internal-server-error";
	}

}

int xmpp_stanza_handler(xmpp_conn_t *conn, xmpp_stanza_t *stanza, void *userdata) {
	LISP_TRY {
		cl_funcall(2, stanza_handler_func, ecl_make_pointer(stanza));
	} LISP_CATCH {
		weechat_printf(NULL, "%sxmpp: lisp stanza handler func failed", weechat_prefix("error"));
	} LISP_END_TRY;
  return TRUE;
}

void
xmpp_conn_cb (xmpp_conn_t *conn, const xmpp_conn_event_t event, const int error, xmpp_stream_error_t *stream_error, void *userdata) {
	cl_object event_type = ECL_NIL;
	char *error_text = NULL;
	char *stream_error_type = NULL;
	char *stream_error_text = NULL;
	switch (event) {
		case XMPP_CONN_CONNECT:
			event_type = eclk("CONNECT");
			xmpp_handler_add(conn, &xmpp_stanza_handler, NULL, NULL, NULL, NULL);
			break;
		case XMPP_CONN_RAW_CONNECT:
			event_type = eclk("RAW-CONNECT");
			break;
		case XMPP_CONN_DISCONNECT:
		case XMPP_CONN_FAIL:
			event_type = eclk("DISCONNECT");
			if (error) {
				error_text = strerror(error);
			}
			else if (stream_error) {
				stream_error_text = stream_error->text;
				stream_error_type = stringify_error_type(stream_error->type);
			}
			xmpp_finish();
			break;
	}
	LISP_TRY {
		cl_funcall(5, support_connect_func, event_type, ecls(error_text), ecls(stream_error_type), ecls(stream_error_text));
	} LISP_CATCH {
		weechat_printf(NULL, "%sxmpp: lisp connect func failed", weechat_prefix("error"));
	} LISP_END_TRY;
}

int
xmpp_connect () {
	if (xmpp_conn) {
		weechat_printf(NULL, "%sxmpp: disconnecting existing connection");
		xmpp_finish();
	}

	xmpp_conn = xmpp_conn_new(xmpp_ctx);
	if (!xmpp_conn) {
		weechat_printf(NULL, "%sxmpp: creating connection failed", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}

	const char *jid = weechat_config_string(opt_jid);
	if (!jid || jid == "") {
		weechat_printf(NULL, "%sxmpp: no JID configured", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}
	const char *password = weechat_config_string(opt_password);
	if (!password || password == "") {
		weechat_printf(NULL, "%sxmpp: no password configured", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}

	xmpp_conn_set_jid(xmpp_conn, jid);
	xmpp_conn_set_pass(xmpp_conn, password);

	weechat_printf(NULL, "xmpp: connecting to server");
	xmpp_connect_client(xmpp_conn, NULL, 0, &xmpp_conn_cb, NULL);

	return WEECHAT_RC_OK;
}

int
xmpp_command_connect (const void *pointer, void *data,
		struct t_gui_buffer *buffer, int argc,
		char **argv, char **argv_eol) {
	return xmpp_connect();
}

int
xmpp_command_disconnect (const void *pointer, void *data,
		struct t_gui_buffer *buffer, int argc,
		char **argv, char **argv_eol) {
	xmpp_finish();
	return WEECHAT_RC_OK;
}

int
xmpp_command_eval (const void *pointer, void *data,
                   struct t_gui_buffer *buffer, int argc,
                   char **argv, char **argv_eol) {
  if (argc < 2) {
    weechat_printf(buffer, "%sxmpp: no form provided", weechat_prefix("error"));
    return WEECHAT_RC_ERROR;
  }
  char *input = argv_eol[1];
  cl_env_ptr env = ecl_process_env();
  cl_object form = ecl_read_from_cstring_safe(input, ECL_NIL);
  if (ecl_eql(form, ECL_NIL)) {
    weechat_printf(buffer, "%sxmpp: nil form or error in reader", weechat_prefix("error"));
    return WEECHAT_RC_ERROR;
  }

  cl_object result = ECL_NIL;
  int failed = 0;

  LISP_TRY {
    result = cl_eval(form);
  } LISP_CATCH {
    failed = 1;
  } LISP_END_TRY;

  if (failed) {
    weechat_printf(buffer, "%sxmpp: evaluation aborted", weechat_prefix("error"));
    return WEECHAT_RC_ERROR;
  }
  char* disp = ecl_get_string_for(result);
  weechat_printf(buffer, "%s", disp);
  return WEECHAT_RC_OK;
}

int do_lisp_load(char *file) {
  int failed = 0;
        ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ECL_T)) {
                cl_load(1, ecls(file));
                weechat_printf(NULL, "xmpp: loaded Lisp %s", file);
        } ECL_HANDLER_CASE(1, condition) {
		char* cond_fmt = ecl_get_string_for(condition);
		failed = 1;
		weechat_printf(NULL, "%sxmpp: could not load Lisp %s: %s", weechat_prefix("error"), file, cond_fmt);
        } ECL_HANDLER_CASE_END;
  return failed ? WEECHAT_RC_OK : WEECHAT_RC_ERROR;
}

int
xmpp_command_load (const void *pointer, void *data,
		struct t_gui_buffer *buffer, int argc,
		char **argv, char **argv_eol) {
	if (argc < 2) {
		weechat_printf(buffer, "%sxmpp: no file path provided", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}
	char *file = argv_eol[1];
	return do_lisp_load(file);
}


void weechat_config_init() {
	struct t_config_file *file = weechat_config_new("xmpp", NULL, NULL, NULL);
	struct t_config_section *sect_account =
		weechat_config_new_section(file, "account", 0, 0,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);
	struct t_config_section *sect_lisp =
		weechat_config_new_section(file, "lisp", 0, 0,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);
	opt_jid =
		weechat_config_new_option(file, sect_account, "jid", "string",
				"The Jabber ID (JID) of the XMPP account to use.",
				NULL, 0, 0, NULL, NULL,
				1, NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);
	opt_password =
		weechat_config_new_option(file, sect_account, "password", "string",
				"The password to authenticate to the XMPP server with.",
				NULL, 0, 0, NULL, NULL,
				1, NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);
	opt_autoconnect =
		weechat_config_new_option(file, sect_account, "autoconnect", "boolean",
				"Whether to connect to XMPP when WeeChat is started.",
				NULL, 0, 0, "off", "off",
				0, NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);
	opt_autoload =
		weechat_config_new_option(file, sect_lisp, "autoload", "string",
				"Semicolon-separated list of files to automatically LOAD when ECL is initialized.",
				NULL, 0, 0, NULL, NULL,
				1, NULL, NULL, NULL,
				NULL, NULL, NULL,
				NULL, NULL, NULL);

	weechat_config_read(file);
}

int on_event_loop_timer(const void *xmpp_ctx_ptr, void *data, int remaining_calls) {
	xmpp_ctx_t *xmpp_ctx = (xmpp_ctx_t *) xmpp_ctx_ptr;
	xmpp_run_once(xmpp_ctx, 0);
	return WEECHAT_RC_OK;
}

int maybe_autoconnect(const void *data1, void *data, int remaining_calls) {
	if (weechat_config_boolean (opt_autoconnect)) {
		xmpp_connect();
	}
	return WEECHAT_RC_OK;
}

cl_object clgc_stanza_alloc() {
	return ecl_make_pointer(xmpp_stanza_new(xmpp_ctx));
}

cl_object clgc_stanza_free(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	return xmpp_stanza_release(stanza) ? ECL_T : ECL_NIL;
}

cl_object clgc_stanza_get_name(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	return ecls(xmpp_stanza_get_name(stanza));
}

cl_object clgc_stanza_clone(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	xmpp_stanza_clone(stanza);
	return ECL_T;
}

cl_object clgc_stanza_set_name(cl_object stanza_ptr, cl_object name_obj) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	char *name = ecl_get_string_for(name_obj);
	return ecl_make_integer(xmpp_stanza_set_name(stanza, name));
}

cl_object clgc_stanza_get_attribute(cl_object stanza_ptr, cl_object attr_obj) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	char *attr = ecl_get_string_for(attr_obj); // a bit allocate-y, but oh well
	return ecls(xmpp_stanza_get_attribute(stanza, attr));
}

cl_object clgc_stanza_set_attribute(cl_object stanza_ptr, cl_object attr_obj, cl_object val_obj) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	char *attr = ecl_get_string_for(attr_obj);
	char *value = ecl_get_string_for(val_obj);
	return ecl_make_integer(xmpp_stanza_set_attribute(stanza, attr, value));
}

cl_object clgc_stanza_get_attributes(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	int count = xmpp_stanza_get_attribute_count(stanza);
	// each attribute has a name + a value
	char **attrs = malloc(count * 2 * sizeof(char *));
	if (!attrs) {
		return ECL_NIL;
	}
	int filled = xmpp_stanza_get_attributes(stanza, attrs, count * 2);
	assert(filled == count * 2);
	cl_object ret = ECL_NIL;
	for (int i = 0; i < count; i++) {
		ret = ecl_cons(ecl_cons(ecls(attrs[i*2]), ecls(attrs[i*2+1])), ret);
	}
	return ret;
}

cl_object clgc_stanza_add_child(cl_object parent_stanza_ptr, cl_object child_stanza_ptr) {
	xmpp_stanza_t *parent_stanza = ecl_foreign_data_pointer_safe(parent_stanza_ptr);
	xmpp_stanza_t *child_stanza = ecl_foreign_data_pointer_safe(child_stanza_ptr);
	return xmpp_stanza_add_child(parent_stanza, child_stanza) ? ECL_T : ECL_NIL;
}

cl_object clgc_stanza_get_children(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
  xmpp_stanza_t *child = NULL;
	cl_object ret = ECL_NIL;

  for (child = xmpp_stanza_get_children(stanza);
       child != NULL; child = xmpp_stanza_get_next(child)) {
		ret = ecl_cons(ecl_make_pointer(child), ret);
  }

	return ret;
}

cl_object clgc_stanza_render(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	char *buf = NULL;
	size_t buflen = 0;
	xmpp_stanza_to_text(stanza, &buf, &buflen);
	return ecls(buf);
}

cl_object clgc_stanza_text_p(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	return xmpp_stanza_is_text(stanza) ? ECL_T : ECL_NIL;
}

cl_object clgc_stanza_get_text(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	return ecls(xmpp_stanza_get_text(stanza));
}

cl_object clgc_stanza_set_text(cl_object stanza_ptr, cl_object text_obj) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	char *text = ecl_get_string_for(text_obj);
	return ecl_make_integer(xmpp_stanza_set_text(stanza, text));
}

cl_object clgc_stanza_send(cl_object stanza_ptr) {
	xmpp_stanza_t *stanza = ecl_foreign_data_pointer_safe(stanza_ptr);
	if (xmpp_conn) {
		xmpp_send(xmpp_conn, stanza);
		return ECL_T;
	}
	else {
		return ECL_NIL;
	}
}

cl_object clgc_printf(cl_object buffer, cl_object string, cl_object err_p) {
	struct t_gui_buffer *buf = NULL;
	if (buffer != ECL_NIL) {
		buf = ecl_foreign_data_pointer_safe(buffer);
	}
        cl_object base = si_coerce_to_base_string(string);
        char* message = base->base_string.self;
	weechat_printf(buf, "%s%s", (err_p == ECL_T) ? weechat_prefix("error") : "", message);
	return ECL_NIL;
}

int weechat_clgc_fd_callback(const void *function, void *data, int fd) {
	cl_object lambda = (cl_object) function;
	int failed = 0;
	LISP_TRY {
		cl_funcall(1, lambda);
	} LISP_CATCH {
		failed = 1;
		weechat_printf(NULL, "%sxmpp: fd %d handler func failed", weechat_prefix("error"));
	} LISP_END_TRY;
	return failed ? WEECHAT_RC_ERROR : WEECHAT_RC_OK;
}

cl_object clgc_hook_fd(cl_object fd, cl_object func) {
	struct t_hook *hook = weechat_hook_fd(ecl_fixnum(fd), 1, 0, 0, &weechat_clgc_fd_callback, func, NULL);
	if (!hook) {
		FEerror("Failed to hook file descriptor ~A.", 1, fd);
	}
	weechat_printf(NULL, "xmpp: hooked file descriptor %d", ecl_fixnum(fd));
	return ecl_make_pointer(hook);
}

cl_object clgc_update_defs() {
	cl_object package = ecl_find_package("WEEXMPP");
	support_connect_func = cl_symbol_function(_ecl_intern("ON-CONNECTION-CHANGED", package));
	stanza_handler_func = cl_symbol_function(_ecl_intern("ON-STANZA-RAW", package));
  log_handler_func = cl_symbol_function(_ecl_intern("ON-STROPHE-LOG", package));
	return ECL_T;
}

void
init_ecl_functions()
{
  si_safe_eval(3, c_string_to_object("(defpackage :wee-impl (:use))"), ECL_NIL, OBJNULL);
  cl_object package = ecl_find_package("WEE-IMPL");
  assert(package != ECL_NIL);
  ecl_def_c_function(_ecl_intern("PRINTF", package), clgc_printf, 3);
  ecl_def_c_function(_ecl_intern("HOOK-FD", package), clgc_hook_fd, 2);
  ecl_def_c_function(_ecl_intern("STANZA-ALLOC", package), clgc_stanza_alloc, 0);
  ecl_def_c_function(_ecl_intern("STANZA-FREE", package), clgc_stanza_free, 1);
  ecl_def_c_function(_ecl_intern("SEND", package), clgc_stanza_send, 1);
  ecl_def_c_function(_ecl_intern("STANZA-REF", package), clgc_stanza_clone, 1);
  ecl_def_c_function(_ecl_intern("STANZA-RENDER", package), clgc_stanza_render, 1);
  ecl_def_c_function(_ecl_intern("STANZA-GET-NAME", package), clgc_stanza_get_name, 1);
  ecl_def_c_function(_ecl_intern("STANZA-SET-NAME", package), clgc_stanza_set_name, 2);
  ecl_def_c_function(_ecl_intern("STANZA-GET-ATTR", package), clgc_stanza_get_attribute, 2);
  ecl_def_c_function(_ecl_intern("STANZA-SET-ATTR", package), clgc_stanza_set_attribute, 3);
  ecl_def_c_function(_ecl_intern("STANZA-LIST-ATTRS", package), clgc_stanza_get_attributes, 1);
  ecl_def_c_function(_ecl_intern("STANZA-ADD-CHILD", package), clgc_stanza_add_child, 2);
  ecl_def_c_function(_ecl_intern("STANZA-GET-CHILDREN", package), clgc_stanza_get_children, 1);
  ecl_def_c_function(_ecl_intern("STANZA-TEXT-P", package), clgc_stanza_text_p, 1);
  ecl_def_c_function(_ecl_intern("STANZA-GET-TEXT", package), clgc_stanza_get_text, 1);
  ecl_def_c_function(_ecl_intern("STANZA-SET-TEXT", package), clgc_stanza_set_text, 2);
  ecl_def_c_function(_ecl_intern("UPDATE-DEFS", package), clgc_update_defs, 0);
}

int weechat_plugin_init(struct t_weechat_plugin *plugin, int argc, char **argv) {
  weechat_plugin = plugin;
	weechat_config_init();
	xmpp_initialize();
	xmpp_ctx = xmpp_ctx_new(NULL, &xmpp_logger);
	if (!xmpp_ctx) {
		weechat_printf(NULL, "%sxmpp: xmpp_ctx_new failed", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}
	ecl_set_option(ECL_OPT_TRAP_SIGSEGV, FALSE);
	ecl_set_option(ECL_OPT_TRAP_SIGFPE, FALSE);
	ecl_set_option(ECL_OPT_TRAP_SIGINT, FALSE);
	ecl_set_option(ECL_OPT_TRAP_SIGILL, FALSE);
	ecl_set_option(ECL_OPT_TRAP_INTERRUPT_SIGNAL, FALSE);
	// ecl is really stupid and reads argv[0] to put in ecl_self.
	// ...why does this even exist?
	char **fake_argv = {"omg-wtf-bbq"};
	cl_boot(0, fake_argv);

	weechat_hook_command("cleval", "Evaluate a Common Lisp form", "[form]", "form: a Common Lisp form", "", &xmpp_command_eval, NULL, NULL);
	weechat_hook_command("clload", "Load a Common Lisp file", "[path]", "path: a file to load", "", &xmpp_command_load, NULL, NULL);
	weechat_hook_command("xconnect", "Connect to the XMPP server", "", "", "", &xmpp_command_connect, NULL, NULL);
	weechat_hook_command("xdisconnect", "Disconnect from the XMPP server", "", "", "", &xmpp_command_disconnect, NULL, NULL);

	struct t_hook *hook = weechat_hook_timer(EVENT_LOOP_INTERVAL, 0, 0, &on_event_loop_timer, xmpp_ctx, NULL);
	if (!hook) {
		weechat_printf(NULL, "%sxmpp: event loop hook failed", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}

	struct t_hook *ac_hook = weechat_hook_timer(1, 0, 1, &maybe_autoconnect, NULL, NULL);
	if (!ac_hook) {
		weechat_printf(NULL, "%sxmpp: autoconnect hook failed", weechat_prefix("error"));
		return WEECHAT_RC_ERROR;
	}

	// Initialize the support library bindings.
  cl_object package = ECL_NIL;

	int failed = 0;
  ECL_HANDLER_CASE_BEGIN(ecl_process_env(), ecl_list1(ECL_T)) {
    init_ecl_functions();
    ecl_init_module(NULL, ecl_support_init);

    package = ecl_find_package("WEEXMPP");
    assert(package != ECL_NIL);
    si_select_package(eclcs("WEEXMPP"));
    si_safe_eval(3, c_string_to_object("(si:install-bytecodes-compiler)"), ECL_NIL, OBJNULL);
    cl_object init_sym = _ecl_intern("INITIALIZE", package);
		cl_object init_func = cl_symbol_function(_ecl_intern("INITIALIZE", package));
		cl_funcall(1, init_func);
  } ECL_HANDLER_CASE(1, condition) {
		char* cond_fmt = ecl_get_string_for(condition);
		weechat_printf(NULL, "%sxmpp: lisp support init failed: %s", weechat_prefix("error"), cond_fmt);
    failed = 1;
  } ECL_HANDLER_CASE_END;

	if (failed) {
		return WEECHAT_RC_ERROR;
	}

	LISP_TRY {
		support_connect_func = cl_symbol_function(_ecl_intern("ON-CONNECTION-CHANGED", package));
		stanza_handler_func = cl_symbol_function(_ecl_intern("ON-STANZA-RAW", package));
		log_handler_func = cl_symbol_function(_ecl_intern("ON-STROPHE-LOG", package));
	} LISP_CATCH {
		weechat_printf(NULL, "%sxmpp: lisp support init failed", weechat_prefix("error"));
		failed = 1;
	} LISP_END_TRY;

	if (failed) {
		return WEECHAT_RC_ERROR;
	}
	const char *autoload = weechat_config_string(opt_autoload);
	if (autoload && autoload != "") {
		char **a_argv;
		int a_argc;

		a_argv = weechat_string_split(autoload, ";", NULL, 0, 0, &a_argc);
		for (int i = 0; i < a_argc; i++) {
			do_lisp_load(a_argv[i]);
		}
		weechat_string_free_split(argv);
	}

	return WEECHAT_RC_OK;
}

int weechat_plugin_end(struct t_weechat_plugin *plugin) {
	xmpp_shutdown();
	cl_shutdown();
	weechat_printf(NULL, "%sxmpp: plugin can not be unloaded", weechat_prefix("error"));
	return WEECHAT_RC_ERROR;
}
