;; TODO(eta): use something like STRING-CASE in this file


;;; Package definitions

(defpackage :wee-impl
  (:use))

(defpackage :strophe-impl
  (:use))

(defpackage :weexmpp
  (:nicknames :wx)
  (:use :cl))

(in-package :weexmpp)

;;; Globals

(defvar *jid* nil
  "Jabber ID (JID) of active connection. NIL if not connected yet.")
(defvar *iq-hash-table* (make-hash-table :test #'equal)
  "Hash table of IQ IDs to closures to run on IQ result.")
(defvar *server-features* nil
  "List of advertised server features, as determined via disco#info.")

(defparameter +ns-bind+ "urn:ietf:params:xml:ns:xmpp-bind")
(defparameter +ns-disco-info+ "http://jabber.org/protocol/disco#info")

;;; Thenables library

(defclass thenable ()
  ((callbacks
    :accessor callbacks
    :initform nil)
   (state
    :initform :pending
    :accessor state)
   (name
    :initarg :name
    :reader name)
   (errbacks
    :accessor errbacks
    :initform nil)
   (resolved-value
    :accessor resolved-value
    :initform nil)
   (rejected-value
    :accessor rejected-value
    :initform nil)))

(defmethod print-object ((obj thenable) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (state callbacks errbacks name) obj
      (format stream "\"~A\" ~A (~A cb, ~A eb)"
              name state (length callbacks) (length errbacks)))))

(defun thenable-resolve (thenable args-list)
  (unless (eql (state thenable) :pending)
    (error "Thenable ~A is already ~A (tried to resolve)"
           thenable (state thenable)))
  (setf (state thenable) :resolved)
  (setf (resolved-value thenable) args-list)
  (dolist (callback (callbacks thenable))
    (apply callback args-list)))

(defun thenable-reject (thenable args-list)
  (unless (eql (state thenable) :pending)
    (error "Thenable ~A is already ~A (tried to reject)"
           thenable (state thenable)))
  (setf (state thenable) :rejected)
  (setf (rejected-value thenable) args-list)
  (dolist (callback (errbacks thenable))
    (apply callback args-list)))

(defmacro with-thenable ((&key resolve reject name) &body body)
  (let ((thenable-sym (gensym "thenable"))
        (args1-sym (gensym "args"))
        (args2-sym (gensym "args"))
        (resolve-sym (or resolve (gensym "resolve")))
        (reject-sym (or reject (gensym "reject")))
        (condition-sym (gensym "condition")))
    `(let ((,thenable-sym
             (make-instance 'thenable
                            :name (or ,name "unnamed thenable"))))
       (labels
           ((,resolve-sym (&rest ,args1-sym)
              (thenable-resolve ,thenable-sym ,args1-sym))
            (,reject-sym (&rest ,args2-sym)
              (thenable-reject ,thenable-sym ,args2-sym)))
            (handler-case
                (progn ,@body)
              (serious-condition (,condition-sym)
                (,reject-sym ,condition-sym))))
       ,thenable-sym)))

(defun thenable-immediate (value)
  (with-thenable (:resolve resolve
                  :name "immediate thenable")
    (resolve value)))

(defun do-then (thenable &optional callback errback)
  (with-thenable (:resolve resolve
                  :reject reject
                  :name (format nil "then: ~A"
                                (name thenable)))
    (when callback
      (let ((next-callback
              (lambda (&rest args)
                (handler-case
                    (let ((result (apply callback args)))
                      (typecase result
                        (thenable
                         (do-then result #'resolve #'reject))
                        (t
                         (apply #'resolve result))))
                  (serious-condition (c)
                    (reject c))))))
        (if (eql (state thenable) :resolved)
            (apply next-callback (resolved-value thenable))
            (push next-callback (callbacks thenable)))))
    (let ((err-callback
            (lambda (&rest args)
              (when errback
                (ignore-errors (apply errback args)))
              (apply #'reject args))))
      (if (eql (state thenable) :rejected)
          (apply err-callback (rejected-value thenable))
          (push err-callback (errbacks thenable))))))

(defun thenable-join (&rest thenables)
  (with-thenable (:resolve resolve
                  :reject reject
                  :name (format nil "join of ~A thenables"
                                (length thenables)))
    (let ((results (make-array (length thenables)
                               :initial-element nil))
          (num-resolved 0)
          (num 0)
          (num-needed (length thenables))
          (err nil))
      (labels
          ((each-callback (nth &rest args)
             (unless err
               (setf (aref results nth) args)
               (when (eql (incf num-resolved) num-needed)
                 (apply #'resolve (coerce results 'list)))))
           (each-errback (&rest args)
             (unless err
               (apply #'reject args))))
        (dolist (thenable thenables)
          (let ((i (1- (incf num))))
            (do-then thenable
              (lambda (&rest args)
                (apply #'each-callback i args))
              #'each-errback)))))))

(defmacro then (thenable lambda-list &body body)
  `(do-then ,thenable
     (lambda ,lambda-list ,@body)))

(defmacro then-err (thenable lambda-list &body body)
  `(do-then ,thenable
     nil
     (lambda ,lambda-list ,@body)))

;;; WeeChat output gray streams

(defclass weechat-output (gray::fundamental-character-output-stream)
  ((to-err
    :initarg :to-err
    :initform nil
    :accessor to-err)
   (buf
    :initform (make-array 16
                          :element-type 'character
                          :adjustable t
                          :fill-pointer 0)
    :accessor buf)))

(defun printf (format-string &rest args)
  (wee-impl::printf nil (apply #'format nil format-string args) nil))

(defmethod gray::stream-write-char ((stream weechat-output) char)
  (with-slots (to-err buf) stream
    (if (char= char #\Newline)
        (when (> (fill-pointer buf) 0)
          (progn
            (if to-err
                (eprintf "xmpp: ~A" buf)
                (printf "xmpp: ~A" buf))
            (setf (fill-pointer buf) 0)))
        (vector-push-extend char buf))))

(setf *standard-output* (make-instance 'weechat-output))
(setf *trace-output* (make-instance 'weechat-output))
(setf *error-output* (make-instance 'weechat-output :to-err t))

(defun eprintf (format-string &rest args)
  (wee-impl::printf nil (apply #'format nil format-string args) t))

(defun initialize ()
  (printf "xmpp: plugin loaded (ECL ~A)" (lisp-implementation-version)))

(defun debugger-handler (condition)
  (eprintf "xmpp: [condition ~A] ~A" (type-of condition) condition)
  (abort))

(defun system::default-debugger (condition)
  (handler-case
      (handler-case
          (debugger-handler condition)
        (t (e)
          (ignore-errors
           (eprintf "xmpp: debugger handler failed: ~A" e))
          (abort)))
    (t (e)
      (si:exit 1))))

(defun on-connection-changed (event-type err stream-err-type stream-err-text)
  (ecase event-type
    (:connect
     (printf "xmpp: connected to server"))
    (:raw-connect
     (printf "xmpp: raw stream connected"))
    (:disconnect
     (cond
       (err (eprintf "xmpp: connection failed: ~A" err))
       ((and stream-err-text stream-err-type)
        (eprintf "xmpp: stream error ~A: ~A" stream-err-type stream-err-text))
       (stream-err-type (eprintf "xmpp: stream error ~A" stream-err-type)))
     (eprintf "xmpp: disconnected from server")
     (setf *jid* nil)
     (setf *server-features* nil))))

(defclass stanza ()
  ((inner
    :initarg :inner
    :accessor inner)))

(defclass text-stanza (stanza)
  ((text
    :initarg :text
    :initform (error "Text required for TEXT-STANZA")
    :reader text)))

(defclass node-stanza (stanza)
  ((name
    :initarg :name
    :initform (error "Name required for NODE-STANZA")
    :reader name)
   (attrs
    :initarg :attrs
    :initform nil
    :reader attrs)
   (children
    :initarg :children
    :initform nil
    :reader children)))

(defun wrap-stanza-ptr (stanza-ptr)
  (wee-impl::stanza-ref stanza-ptr)
  (make-instance 'stanza :inner stanza-ptr))

(defun send (stanza)
  (wee-impl::send (inner stanza)))

(defun finalize-stanza (stanza)
  (wee-impl::stanza-free (inner stanza)))

(defun render-stanza (stanza)
  (wee-impl::stanza-render (inner stanza)))

(defmethod initialize-instance :before ((instance stanza) &rest args)
  (declare (ignore args))
  #+ecl (ext:set-finalizer instance #'finalize-stanza))

(defmethod initialize-instance :after ((instance stanza) &rest args)
  (declare (ignore args))
  (when (slot-boundp instance 'inner)
    (let ((inner (inner instance)))
      (if (wee-impl::stanza-text-p inner)
          (change-class instance 'text-stanza
                        :text (wee-impl::stanza-get-text inner))
          (change-class instance 'node-stanza
                        :name (wee-impl::stanza-get-name inner)
                        :attrs (wee-impl::stanza-list-attrs inner)
                        :children
                        (mapcar #'wrap-stanza-ptr
                                (wee-impl::stanza-get-children inner)))))))

(defmethod initialize-instance :after ((instance text-stanza) &rest args)
  (declare (ignore args))
  (unless (slot-boundp instance 'inner)
    (let ((inner (setf (inner instance) (wee-impl::stanza-alloc))))
      (wee-impl::stanza-set-text inner (text instance)))))

(defmethod initialize-instance :after ((instance node-stanza) &rest args)
  (declare (ignore args))
  (unless (slot-boundp instance 'inner)
    (let ((inner (setf (inner instance) (wee-impl::stanza-alloc))))
      (wee-impl::stanza-set-name inner (name instance))
      (loop
        for (k . v) in (attrs instance)
        do (wee-impl::stanza-set-attr inner k v))
      (dolist (child (children instance))
        (wee-impl::stanza-add-child inner (inner child))))))

(defmethod print-object ((obj node-stanza) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (name attrs children) obj
      (format stream "\"~A\" (~A attrs, ~A children)"
              name (length attrs) (length children)))))

(defmethod print-object ((obj text-stanza) stream)
  (print-unreadable-object (obj stream :type t)
    (with-slots (text) obj
      (format stream "\"~A\""
              text))))

(defun go-fmt-stanza (out stanza &optional (level 0))
  (let ((level-str (format nil "~v@{~A~:*~}" level #\Space)))
    (when (typep stanza 'text-stanza)
      (format out "~Atext: ~A~%" level-str (text stanza))
      (return-from go-fmt-stanza))
    (format out "~A\"~A\" ~A~%" level-str (name stanza) (attrs stanza))
    (dolist (child (children stanza))
      (go-fmt-stanza out child (+ level 1)))))

(defun fmt-stanza (stanza)
  (with-output-to-string (out)
    (go-fmt-stanza out stanza)))

(defvar *current-attrs* :sentinel)
(defvar *current-children* :sentinel)

(defun stanza-attr (name value)
  "Add NAME=VALUE to the list of attributes in the current stanza being created by STANZAIZE."
  (when (eql *current-attrs* :sentinel)
    (error "STANZA-ATTR called outside STANZAIZE"))
  (push (cons name value) *current-attrs*))

(defun stanza-text (text)
  "Add text to the body of the current stanza being created by STANZAIZE."
  (when (eql *current-children* :sentinel)
    (error "STANZA-TEXT called outside STANZAIZE"))
  (push (make-instance 'text-stanza :text text) *current-children*))

(defmacro stanzaize ((name) &body body)
  (let ((attrs-name (gensym "attrs"))
        (children-name (gensym "children"))
        (prev-children-name (gensym "prev-children"))
        (stanza-name (gensym "stanza")))
    `(let
         ((,stanza-name
            (let ((*current-children* '())
                  (*current-attrs* '()))
              ,@body
              (make-instance 'node-stanza
                             :name ,name
                             :attrs *current-attrs*
                             :children *current-children*))))
       (if (eql *current-children* :sentinel)
           ,stanza-name
           (push ,stanza-name *current-children*)))))

;; Stanza destructuring primitives

(defun text-stanza-p (stanza)
  "Returns T if STANZA is a TEXT-STANZA."
  (typep stanza 'text-stanza))

(defun s-text (stanza)
  "Gets the text body of the provided STANZA object (i.e. the text associated with the stanza object that is the only child of STANZA), or NIL if there is none."
  (when stanza
    (let ((first-child (first (children stanza))))
      (when (and first-child (text-stanza-p first-child))
        (text first-child)))))

(defun s-child (name stanza)
  "Returns the child of STANZA named NAME, or NIL if none exists."
  (when stanza
    (car
     (remove-if-not
      (lambda (stanza)
        (and (not (text-stanza-p stanza))
             (string= (name stanza) name)))
      (children stanza)))))

(defun s-attr (attr stanza)
  "Return the value of the ATTR on STANZA, or NIL if it does not exist."
  (cdr (assoc attr (attrs stanza)
              :test #'equal)))

(defun s-attr-is (attr value stanza)
  "Returns T if the value of ATTR on STANZA is VALUE."
  (string= (s-attr attr stanza) value))

(defun s-child-xmlns (name xmlns stanza)
  "Returns the child of STANZA named NAME with the provided XMLNS, or NIL if none exists."
  (when stanza
    (car
     (remove-if-not
      (lambda (stanza)
        (and (not (text-stanza-p stanza))
             (string= (name stanza) name)
             (string= (s-attr "xmlns" stanza) xmlns)))
      (children stanza)))))

(defun s-name-is (name stanza)
  "Returns T if NAME is STANZA's name."
  (string= (name stanza) name))

(defun maybe-destructure-bind-stanza (stanza)
  "If STANZA is an IQ bind result, return the JID contained therein (else NIL)."
  (s-text (s-child "jid" (s-child-xmlns "bind" +ns-bind+ stanza))))

(defun update-presence ()
  (send
   (stanzaize ("presence"))))

(defun generate-id ()
  (format nil "~A-~A"
          (random (expt 2 32))
          (get-universal-time)))

(defmacro iq-get ((&key to id) &body body)
  (let ((stanza-id-sym (gensym "stanza-id"))
        (thenable-sym (gensym "thenable")))
    `(let* ((,stanza-id-sym (or ,id (generate-id)))
            (,thenable-sym
              (make-instance 'thenable
                             :name (format nil "IQ ~A" ,stanza-id-sym))))
       (setf (gethash ,stanza-id-sym *iq-hash-table*)
             ,thenable-sym)
       (send
        (stanzaize ("iq")
          ,@(when to
              `((stanza-attr "to" ,to)))
          (stanza-attr "from" *jid*)
          (stanza-attr "id" ,stanza-id-sym)
          (stanza-attr "type" "get")
          ,@body))
       ,thenable-sym)))

(defun bare-jid (jid)
  "Strips the resource component from JID, if there is one."
  (let ((slash (position #\/ jid :from-end t)))
    (if slash
        (subseq jid 0 slash)
        jid)))

(defun jid-hostname (jid)
  "Extracts the hostname component from JID."
  (let* ((jid (bare-jid jid))
         (at (position #\@ jid)))
    (if at
        (subseq jid (1+ at))
        jid)))

(defun disco-info (entity)
  (then
      (iq-get (:to entity)
        (stanzaize ("query")
          (stanza-attr "xmlns" +ns-disco-info+)))
      (stanza)
    (let ((query-child (s-child "query" stanza))
          (ret nil))
      (dolist (child (children query-child))
        (when (string= (name child) "feature")
          (push (s-attr "var" child) ret)))
      ret)))

(defun update-server-features ()
  (then
      (thenable-join
       (disco-info (jid-hostname *jid*))
       (disco-info (bare-jid *jid*)))
      (server self)
    (let ((both (append server self)))
      (printf "xmpp: identified ~A disco#info features (~A server, ~A self)"
              (length both) (length server) (length self))
      (setf *server-features* both))))

;; Stanza handling

;; NOTE(eta): ON-STANZA-RAW must be recompiled to pick up changes to this
;;            function (and see the note on that function)
(defun on-stanza (stanza)
  (cond
    ((and
      (s-name-is "iq" stanza)
      (s-attr-is "id" "_xmpp_bind1" stanza))
     (let ((bound-jid (maybe-destructure-bind-stanza stanza)))
       (if bound-jid
           (progn
             (setf *jid* bound-jid)
             (update-presence)
             (update-server-features)
             (printf "xmpp: bound to JID ~A" bound-jid))
           (eprintf "xmpp: failed to destructure bind stanza! rendered ~A"
                    (render-stanza stanza)))))
    ((and
      (s-name-is "iq" stanza)
      (s-attr-is "type" "result" stanza))
     (let* ((id (s-attr "id" stanza))
            (thenable (gethash id *iq-hash-table*)))
       (thenable-resolve thenable (list stanza))
       (remhash id *iq-hash-table*)))
    ((and
      (s-name-is "iq" stanza)
      (s-attr-is "type" "error" stanza))
     (eprintf "xmpp: IQ error response: ~A" (render-stanza stanza)))
    (t
     (format *error-output* "unexpected stanza in the bagging area:~%~A~%"
             (fmt-stanza stanza)))))

;; NOTE(eta): run (WEE-IMPL::UPDATE-DEFS) after changing this function
(defun on-stanza-raw (stanza-ptr)
  (on-stanza (wrap-stanza-ptr stanza-ptr)))

(defun on-strophe-log (level area msg)
  (declare (ignore level))
  (unless (or
           (search "SENT:" msg)
           (search "RECV:" msg))
    (format t "~A~A~%"
            msg
            (if (string= area "xmpp")
                ""
                (format nil " (~A)" area)))))
