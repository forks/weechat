(load "~/.emacs.d/elpa/slime-20200611.1146/swank-loader.lisp" :verbose t)

(swank-loader:init)

(in-package :swank)

(defvar *fd-handlers* (make-hash-table))

(defun hook-socket (socket fun)
  (wee-impl::hook-fd
   (sb-bsd-sockets:socket-file-descriptor socket)
   fun))

(defun accept (socket)
  "Like socket-accept, but retry on EAGAIN."
  (loop (handler-case
            (return (sb-bsd-sockets:socket-accept socket))
          (sb-bsd-sockets:interrupted-error ()))))

(defun accept-connection (socket)
  (let ((fd (accept socket)))
    (values
     (sb-bsd-sockets:socket-file-descriptor fd)
     (sb-bsd-sockets:socket-make-stream fd
                                        :output t
                                        :input t
                                        :buffering :full
                                        :element-type '(unsigned-byte 8)
                                        :external-format nil))))

(defun accept-connections (socket style dont-close)
  (multiple-value-bind (fd client)
      (accept-connection socket)
    (format t "Accepted SWANK connection on fd ~A~%" fd)
    (authenticate-client client)
    (let ((connection (make-connection socket client style)))
      (setf
       (gethash fd *fd-handlers*)
       (wee-impl::hook-fd
        fd
        (lambda ()
          (handle-requests connection t))))
      (serve-requests (make-connection socket client style)))))


(defun install-fd-handler (connection)
  (handle-requests connection t))

(swank::defimplementation swank::add-fd-handler (socket fun)
  (hook-socket socket fun))

(swank::defimplementation swank::remove-fd-handlers (socket)
  (format t "Not bothering to remove fd handlers for ~A~%" socket))

(swank:create-server :style :fd-handler)
