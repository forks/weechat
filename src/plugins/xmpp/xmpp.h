/*
 * Copyright (C) 2021 eta <git@eta.st>
 *
 * This file is part of WeeChat, the extensible chat client.
 *
 * WeeChat is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * WeeChat is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with WeeChat.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef WEECHAT_PLUGIN_XMPP_H
#define WEECHAT_PLUGIN_XMPP_H

#define weechat_plugin weechat_xmpp_plugin
#define XMPP_PLUGIN_NAME "xmpp"

extern struct t_weechat_plugin *weechat_xmpp_plugin;

// from support.lisp
extern void ecl_support_init(cl_object);

#define ecls(x) (x ? ecl_make_simple_base_string(x,-1) : ECL_NIL)
#define eclk(x) (x ? ecl_make_keyword(x) : ECL_NIL)
#define eclcs(x) (x ? ecl_make_constant_base_string(x,-1) : ECL_NIL)

#endif
